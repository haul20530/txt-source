利維伊破碎的身體一邊飄在半空，一邊焚燒著。利維伊教徒們，儘管險些被他們的教神殺掉，依然留著眼淚地、呆呆地守望著漸漸消失的利維伊。

「真是個既噁心又頑強的傢伙呢，不過現在終於結束了……」

佩特羅很疲勞似的說道。對此，我無言地點頭。

『還沒完……還沒、結束……』

聖都裡回蕩著利維伊的聲音。這實在是令我很驚訝。

聖都的中央，破破爛爛的利維伊的頭部，以及緊緊抓住薩特利亞的巨大手臂在飄浮著。它的三只眼睛像是拼貼遊戲一樣，在臉上不停地變換位置。它張大的嘴巴發出難聽的嗚咽聲，裡面出現了第四只巨大的眼珠。

「……那、那個是什麼啊，佩特羅先生？」

在這衝擊性的景象面前，我緊握著魔杖，心裡只有驚訝。佩特羅也張著嘴巴，呆然地凝視著利維伊的頭。他也不知道發生了什麼事吧。也對，換做我也難以回答這個問題。

在空中飛舞的利維伊的頭，與抓著薩特利亞的手臂結合到一起。這景象無法用語言來形容，就像是噩夢一樣，令人相當的不舒服。接著，利維伊的鬍鬚和頭髮伸長，相互纏繞成棒狀，形成了好幾根長長的手臂一樣的東西。或許是因為那很長，像肘一樣彎曲，所以顯得有點奇妙地生動。

思緒回轉，有件事非常奇怪。

為何，作為利維伊的半身的龍脈，利維伊自己卻無法運用呢。而且為何，只有具備侍奉利維伊的巫女之血的人，才能夠制御龍脈呢。說來，在神話時代裡老早就被滅的利維伊，如今因為什麼原因而復活，直到現在一直都沒有人討論。

我也沒有詳細調查的機會，把這些當作平平無奇就再沒深究。然而，仔細思考的話，這些事情的組合，實在是非常奇怪。

惡魔假冒為神，在這個世界裡明明是司空見慣的事情，為什麼神格威嚴的利維伊會是真貨，而我卻對此深信不疑呢。

『事已至此，繼續隱瞞真身也沒有意義了……。余為米拓，乃侍奉四大創造神中最強著稱的空神的八十八天使之統率，四大天使的最後留存者，在空神的身姿消失後一萬年中，毀滅無數宗教與國家的，被稱為『世界之盤踞者』的最高位惡魔，偽神米拓是也……！』

臉上荒誕奇異的四只眼睛旁邊，八根手臂彎曲地搖晃著。猶如是描繪在意圖不明的抽象畫上的，太陽一樣的姿態。

『在余不能暴露真身的時候，魔法的使用受到限制，但現在的話，余不必再克制。葬送無數國家的余之魔法，再加上利維伊的龍脈力量。從現在開始，就是真正的戰鬥……』

我實在是太過驚訝，嘴巴張開又閉上。不過，教徒們受到的衝擊遠在我之上。

「你、你、連真貨都不是麼……」

它的行為既沒有正名，力量也是借來之物。但即使如此它依然被認為是真貨，成為了利維拉斯國的希望，薩特利亞也追隨於它。現在，這一切都連根拔起了。而且本來新利維伊派就沒有任何的正當性，僅僅是個恐怖組織而已。

『哈，這個女人，你們國家的所有人，全是愚蠢之徒！居然將聖地讓渡給余，還犧牲自己把龍脈的魔力也獻上呢！然而，說到底也是四大創造神的最弱！用這種小孩一樣的魔術就能讓你們滿足、屈服！動彈不得了麼，亞貝爾。就讓你領教一下真正的絕望吧！』

發光的絲線纏繞在利維伊的頭部，將其包裹，形成了巨大的絲繭。絲繭繼續膨大，長出觸手，伸出了一個長頭髮的人類上半身。觸手的其中一根正束縛著薩特利亞。人類上半身沒有正常的臉，像之前的米拓一樣，四只眼和鼻子荒誕地配置在頭和身體的不同地方。

『怎麼，嚇到了嗎？哈哈哈哈，看啊，像是大邪神庫多爾一樣！余運用龍脈，即便是大邪神庫多爾的力量也能夠再現出來！稍微有點魔力量不足，在薩特利亞力盡、龍脈乾涸之前，盡情戰鬥吧！』

米拓張開雙臂。伴隨著它的動作，米拓周圍展開了像是巨大的發光蜘蛛網一樣的事物。果然，不自然的切割攻擊、薩特利亞的傀儡化，以及使用絲繭的身體變形，它很擅長使用魔力之絲的魔法。蜘蛛網恐怕是將靠近之物束縛的結界之類的東西吧。

我看向利維伊教徒們。他們都失魂落魄地倒在地上。這也情有可原，這個國家，在米拓拿著槍假扮利維伊的時候開始，就已經完蛋了。

「你……真的應該收斂一下了。到底要玩弄人心到什麼地步……」

『呵，現在哭著跪求已經遲了！亞貝爾，就是你小子讓余一切都白費了！在這個世界最強的暴力面前，化作腐朽吧！在最後，余之名將深刻地、深刻地刻印在神話之中！成為永恒不滅的恐怖！』

「……সমন 召喚」

我在月神上面，平靜地揮舞魔杖。我胸上的召喚紋發出光芒，巨大的魔法陣浮現在我與米拓的中間。

『居然是召喚魔術，可笑！再愚蠢也要有限度！對於掌控著利維伊之龍脈、以及庫多爾之姿態的余，到底要召喚什麼樣的惡魔來對抗？臨死也要惡作劇麼，亞貝……』

---

魔法陣的中央，與現在的米拓有著相似姿態的人工精靈，庫多爾現身了。僅僅是出現了輪廓，其迫力就迥然不同。庫多爾身上散發出的邪氣，讓周圍空氣劇變。她降落到地上的瞬間，天空的顏色開始變成紅黑色，遠處傳來雷鳴。

『吾之魔力十分貴重……因此，吾說過不可隨意召喚，現在是何事？』

庫多爾下半身的巨大的單眼瞪視著我。

「……不好意思，稍微有點緊要的事呢。這個分量的魔力的話，我必會在別處的貢獻上補償的」

『哼，嘛，好吧。與酋雷姆有過接觸的惡魔的話，也不虛此行。而且，它不是有著挺有趣的扮裝麼。做這種荒唐的事情，到底意欲為何？』

庫多爾的單眼直直看向米拓。

『不、不可能，庫多爾已經甦醒了嗎！？這、怎麼會……酋雷姆沒有說過這個……！不、不是，不可能是庫多爾，這種故弄玄虛的手段，對余是不管用的！而且，那個暴虐的化身庫多爾，怎麼可能會將召喚紋給予約哈南以外的人！』

庫多爾拖著巨大的身體，慢慢前進，逼近了站在水道中央的瓦礫之山上的米拓。

『無、無聊的假貨！退散吧！』

米拓舉起雙臂。無數浮現著龍脈術式的水彈出現在半空，襲向庫多爾。庫多爾伸出前面的觸手，交纏起來抵擋。觸手依次把水彈彈開。

『怎麼？利維伊的魔彈多少會有點難纏呢。這種嬉鬧一樣的攻擊，就是你的極限麼？』

庫多爾前進的速度稍稍加快。

『蠢、蠢貨，竟然馬虎地接近過來！得手了！』

米拓周圍飄浮著的巨大蜘蛛網一個接一個地消失，下一瞬間，庫多爾的身體被無數的蜘蛛網黏住，固定在地上。

『成、成功了！呼呼呼、哈哈哈哈哈！這就是余的結界魔法……』

伴隨著巨大的打擊聲，束縛著庫多爾的絲線被粉碎，地面出現巨大的坑洞。跳起來的庫多爾直接靠近米拓，用觸手纏住其頭部，舉起來往地上一砸。接著再次舉起來，在半空中纏住。米拓的人形部分被觸手撕碎。

『啊啊、啊——————！』

人形發出臨終的嘶叫，放開了用觸手拘束著的薩特利亞的身體，薩特利亞掉到水道裡。米拓的下腹部部分柔軟地凹下，從觸手之間擠過去逃了出來。

『開、開什麼玩笑，怎麼可能跟這種怪物戰鬥！』

庫多爾的觸手毫不留情地從上方打擊米拓的下腹部。米拓的肉塊伸出來的絲觸手被打得痙攣不止。

『喂你這傢伙，說出真正的名字。那樣的話就饒你一命。難不成到了現在，還想假充吾之名』

『米、米拓、是米拓！』

米拓很痛苦的把自己的觸手往地面拍打。肉塊裂開，收縮，回復到了原本的四只眼睛、奇異的太陽繪畫一樣的姿態。

『不、不是這個』

『哈、哈啊！？』

『你這傢伙，是青蛙。僅僅是青蛙』

一時間，米拓像是無法理解一樣僵住了。不過，它像是明白了什麼一樣，眼珠動了動。無數的光之絲線包裹住米拓，將它壓迫、縮小。之後，它變成了四只眼睛、全身藍色的青蛙的姿態。

『嗯哼，這樣就好』

不知道什麼是這樣就好，庫多爾滿足似的點頭。青蛙跳起來，從庫多爾那裡逃跑。她毫不留情地對其背後揮出觸手一擊。青蛙的肉四處飛散。

『卑劣的傢伙』

庫多爾抱起手，在宣告勝利似的低聲說道。

「這，情報……不就問不出來了嗎……」

我小聲說道，庫多爾看向青蛙的碎片。殘骸已經無法保持精靈體，開始消散。

『……在下次見面之前，你把它治好』

「別、別強人所難啊！這我做不到啊！這個已經，完全失去了自我意識……！」

庫多爾單方面地把難題強加給我之後，當場展開魔法陣，消失了踪影。
